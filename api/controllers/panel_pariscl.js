const moment = require('moment-timezone')
const bigquery = require('@google-cloud/bigquery')({
	  projectId: 'txd-paris-johnson',
	  keyFilename: '.bigquery_pariscl_paneles.json'
})

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
exports.getPanelByDataRange = (req, res) => {
	let listValues		 = req.swagger.params.listValues.value ? req.swagger.params.listValues.value : `'DECO-HOGAR',  'ELECTRO-HOGAR', 'TECNOLOGIA', 'ACCESORIOS',  'DEPORTES', 'HOMBRES',  'INFANTIL',  'MUJER'`
  const dateStarting = req.swagger.params.dateStarting.value ? moment(req.swagger.params.dateStarting.value) : null
  const dateEnding   = req.swagger.params.dateEnding.value ? moment(req.swagger.params.dateEnding.value) : null

if(typeof listValues ==undefined)
listValues=`'DECO-HOGAR',  'ELECTRO-HOGAR', 'TECNOLOGIA', 'ACCESORIOS',  'DEPORTES', 'HOMBRES',  'INFANTIL',  'MUJER'`


	const panelQuery2 =
                            `#standardSQL
select competencia_SKU,competencia_precio,retail,z.sku as sku, z.ultima_actualizacion as ultima_actualizacion,z.precio_normal as precio_normal,z.precio_oferta as precio_oferta,z.precio_tarjeta as precio_tarjeta,z.product_views as product_views,z.product_views_acum as product_views_acum,z.venta_neta as venta_neta,z.venta_neta_acum as venta_neta_acum,
			 z.unidades as unidades, z.unidades_acum as unidades_acum,z.unidades_tarjeta as unidades_tarjeta,z.unidades_tarjeta_acum as unidades_tarjeta_acum,z.penetracion as penetracion,z.penetracion_acum as penetracion_acum,z.precio_venta_promedio as precio_venta_promedio,z.precio_venta_promedio_acum as precio_venta_promedio_acum,
			 z.conversion as conversion,z.conversion_acum as conversion_acum,z.descripcion as descripcion,z.depto as depto,z.subdepto as subdepto,z.clase as clase,z.marca as marca,z.img_prod_1 as img_prod_1,z.vd as vd,z.bopis as bopis,z.url as url,z.division as division,z.fecha as fecha,z.precio_normal_medio_acum as precio_normal_medio_acum,
			 z.precio_oferta_medio_acum as precio_oferta_medio_acum,z.bdocientos as bdocientos,z.bdoce as bdoce,z.margen1_acum as margen1_acum,z.margen1 as margen1,z.contribucion_pesos_acum as contribucion_pesos_acum,z.contribucion_pesos as contribucion_pesos
from (select                x1.sku as sku,
														m1.timestamp as ultima_actualizacion,
														ifNull(m1.precioNormal,0) as precio_normal,
														ifNull(m1.precioOferta,0) as precio_oferta,
														ifNull(m1.precioTarjeta,0) as precio_tarjeta,
														ifNull(m1.pv,0) as product_views,
														ifNull(x1.views_acum,0) as product_views_acum,
														ifNull(m1.precioVenta_prom*m1.unidades,0) as venta_neta,
														ifNull(x1.venta_neta_acum,0) as venta_neta_acum,
														ifNull(m1.unidades,0) as unidades,
														ifNull(x1.unidades_acum,0) as unidades_acum,
														ifNull(m1.unidades_t,0) as unidades_tarjeta,
														ifNull(x1.unidades_tarjeta_acum,0) as unidades_tarjeta_acum,
														ifNUll(m1.penetracion_tarjeta,0) as penetracion,
														ifNull(x1.penetracion_acum,0) as penetracion_acum,
														ifNull(m1.precioVenta_prom,0) as precio_venta_promedio,
														ifNull(x1.precio_Venta_prom_acum,0) as precio_venta_promedio_acum,
														ifNull(m1.conversion,0) as conversion,
														ifNull(x1.conversion_acum,0) as conversion_acum,
														ifNull(m1.descripcion,'') as descripcion,
														ifNull(m1.depto,'')as depto,
														ifNull(m1.subdepto,'') as subdepto,
														ifNull(m1.clase,'') as clase,
														ifNull(m1.marca,'') as marca,
														ifNull(m1.imgProd1,'') as img_prod_1,
														ifNull(m1.cantidad_vd,0) as vd,
														ifNull(m1.cantidad_bopis,0) as bopis,
														ifNull(m1.url,'') as url,
														ifNull(m1.division,'') as division,
														m1.m_fecha_corregida as fecha,
														ifNull(precio1.precioNormal_medio_acum,0) as precio_normal_medio_acum,
														ifNull(precio1.precioOferta_medio_acum,0) as precio_oferta_medio_acum,
														ifNull(m1.b200,0) as bdocientos,
														ifNull(m1.b12,0) as bdoce,
														round(x1.contribucion_acum/(x1.venta_neta_acum)*100,1) as margen1_acum,
														round((costo.contribucion/(m1.precioVenta_prom*m1.unidades))*100,1) as margen1,
														round(x1.contribucion_acum) as contribucion_pesos_acum,
														round(costo.contribucion) as contribucion_pesos

														from
														(select sku, sum(pv) as views_acum, sum(precioVenta_prom*unidades)  as venta_neta_acum,  sum(unidades) as unidades_acum,
														round(sum(unidades)/sum(pv)*100,2) as conversion_acum, cast(sum(precioVenta_prom*unidades)/sum(unidades) as INT64) as precio_venta_prom_acum,
														sum(unidades_t) as unidades_tarjeta_acum, round((sum(unidades_t)/sum(unidades))*100,2) as penetracion_acum, sum(contribucion) as contribucion_acum
														from dataAgrupadaParis.kpis where fecha_corregida BETWEEN '${dateStarting.format('YYYY-MM-DD')}' AND '${dateEnding.format('YYYY-MM-DD')}'  group by sku) x1
														left join
														(select estilo, cantidad_vd,
														cantidad_bopis, timestamp,
														descripcion, depto,
														subdepto, clase,
														precioNormal, precioOferta,
														precioTarjeta, marca,
														url, case when precioDetalle.division is null then combos.division else precioDetalle.division end as division,
														m_fecha_corregida, pv,
														precioVenta_prom, unidades,
														unidades_t, penetracion_tarjeta,
														conversion, imgprod1,
														b200, b12
														from maestra.precioDetalle left join
														maestra.combos on combos.sku=precioDetalle.estilo) m1
														on x1.sku=m1.estilo
														left join
														(select sku, cast(sum(precioMedioNormal)/count(precioMedioNormal) AS INT64) as precioNormal_medio_acum,
														cast(sum(precioMedioOferta)/count(precioMedioOferta) as INT64) as precioOferta_medio_acum
														from dataAgrupadaParis.precios_medios_promedio WHERE fecha BETWEEN '${dateStarting.format('YYYY-MM-DD')}' AND '${dateEnding.format('YYYY-MM-DD')}'
														group by sku) precio1

														on x1.sku=precio1.sku

														left join
														(select sku, contribucion from dataAgrupadaParis.kpis where fecha_corregida = (select max(fecha_corregida) from dataAgrupadaParis.kpis ) group by sku, contribucion) costo
														on x1.sku=costo.sku

														where  m1.m_fecha_corregida is not null and m1.precioNormal>0 and division ='${listValues}'
														group by x1.sku, x1.views_acum,
														m1.cantidad_vd, m1.cantidad_bopis,      m1.timestamp,   m1.descripcion, m1.depto,       m1.subdepto,   m1.clase,        m1.precioNormal,        m1.precioOferta, m1.precioTarjeta,       m1.marca,
														m1.url, m1.imgProd1,    m1.division,    m1.m_fecha_corregida,   m1.pv,  x1.precio_Venta_prom_acum,     m1.unidades,     m1.conversion,x1.conversion_acum,x1.venta_neta_acum,
														x1.unidades_acum,m1.precioVenta_prom,venta_neta,x1.unidades_tarjeta_acum, penetracion_acum,m1.unidades_t,m1.penetracion_tarjeta, m1.b200, m1.b12, precio_normal_medio_acum,precio_oferta_medio_acum,
														margen1_acum,margen1,contribucion_pesos,contribucion_pesos_acum
                            order by x1.sku) z left join

(select paris_sku, competencia_sku, competencia_precio, retail from(select distinct
    row_number()  OVER(PARTITION BY x.sku) as diff,
    case when substr(x.sku,1,4) like "skul%" then substr(x.sku,5,11)
     when substr(x.sku,1,3) like "sku%"  then substr(x.sku,4,10)
     when x.sku like "%-%" then substr(x.sku,1,(STRPOS(x.sku,"-")-1))
     else x.sku
    end AS paris_SKU
     , case when y.sku like "%P%" then substr(y.sku,1,(STRPOS(y.sku,"P")-1))
     else y.sku end  as competencia_SKU
     ,x.price as paris_Precio
     ,y.price as competencia_Precio
     ,y.store_name as Retail
from dataAgrupadaParis.pricing_compass_Paris x
 ,dataAgrupadaParis.pricing_compass_Paris y
 ,dataAgrupadaParis.pricing_compass_Matching
where x.product_id = paris_product_id
and y.product_id = competitor_product_id
and x.store_name='Paris'
group by x.sku
            ,y.sku
     ,x.price
     ,y.price
            ,y.store_name
) as t 
where t.diff = 1
order by paris_sku)
on  z.sku= paris_sku
order by competencia_SKU desc`












  //const condDateStarting = `fecha >= "${dateStarting ? dateStarting.format('YYYY-MM-DD'):null}" `
  //const condDateEnding = `fecha <"${dateEnding ? dateEnding.format('YYYY-MM-DD'):null}" `
  //const fixedQuery =  `${panelQuery2}
	//${dateStarting || dateEnding ? 'WHERE ': ''}${dateStarting ? condDateStarting:''}${dateStarting && dateEnding ? 'AND ':''}${dateEnding ? condDateEnding :''}
	//${sortQuery}`

console.log(panelQuery2)
  bigquery.query({
    query: panelQuery2,
    maxResults: 300000
  }).then((ans) => {res.json(ans[0])}).catch(e => {
	  res.status(500, e.message)
	  //throw e
  })
}

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
